/* window.vala
 *
 * Copyright 2022 Cosmin Humeniuc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace ShoppingList {

    [GtkTemplate (ui = "/ro/hume/cosmin/ShoppingList/window.ui")]
    public class Window : Gtk.ApplicationWindow {

        [GtkChild]
        private unowned Gtk.SearchEntry entry;

        [GtkChild]
        private unowned Gtk.Button clear;

        [GtkChild]
        private unowned Gtk.ListView list;

        private GLib.ListStore list_model;

        private GLib.Settings settings;

        private DbAccessor db_accessor;

        public Window (Gtk.Application app) {
            Object (application: app);
        }

        construct {
            db_accessor = new DbAccessor ();
            setup_list ();

            entry.activate.connect ((entry) => {
                int64 id = db_accessor.add_item (entry.text);
                list_model.append (new Item.with_id_and_name (id, entry.text));
                entry.text = "";
            });
            clear.clicked.connect (() => {
                var item_count = list_model.get_n_items ();
                for (uint i = item_count; i > 0; i--) {
                    var index = i - 1;
                    var list_item = list_model.get_item (index);
                    if (list_item != null) {
                        var item = list_item as Item;
                        if (item.state == Item.State.CHECKED) {
                            print ("Removing %" + int64.FORMAT + " = %s\n", item.id, item.name);
                            db_accessor.delete_item (item.id);
                            list_model.remove (index);
                        }
                    }
                }
            });

            settings = new GLib.Settings ("ro.hume.cosmin.ShoppingList");
            set_default_size (settings.get_int ("window-width"), settings.get_int ("window-height"));
            close_request.connect (e => {
                return before_destroy ();
            });
        }

        private void setup_list () {
            list_model = new GLib.ListStore (typeof (Item));
            var rows = db_accessor.get_all_items ();
            foreach (var row in rows) {
                ShoppingList.Item.State state = ShoppingList.Item.State.value_of (row.state);
                list_model.append (new Item.with_id_name_and_state (row.id, row.name, state));
            }

            var sel = new Gtk.NoSelection (list_model);

            var factory = new Gtk.SignalListItemFactory ();
            factory.setup.connect ((object) => {
                var row = new Row ();
                Gtk.ListItem list_item = (Gtk.ListItem) object;
                list_item.set_child (row);
            });
            factory.bind.connect ((object) => {
                Gtk.ListItem list_item = (Gtk.ListItem) object;
                Row row = (Row) list_item.get_child();
                Item item = (Item) list_item.get_item();
                row.set_text (item.name);
                row.set_state (item.state);
                row.toggled.connect ((active) => {
                    print ("toggled row for id %" + int64.FORMAT + " to %s\n", item.id, active ? "on" : "off");
                    db_accessor.update_item_state (item.id, active);
                    item.state = active ? ShoppingList.Item.State.CHECKED : ShoppingList.Item.State.UNCHECKED;
                });
            });

            list.factory = factory;
            list.model = sel;
        }

        private bool before_destroy () {
            int w = get_width ();
            int h = get_height ();
            settings.set_int ("window-width", w);
            settings.set_int ("window-height", h);
            return false;
        }
    }
}
