/* row.vala
 *
 * Copyright 2022 Cosmin Humeniuc <cosmin@hume.ro>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


 namespace ShoppingList {

    public class Row : Gtk.Box {

        private Gtk.CheckButton check_button;
        private Gtk.Label label;

        public Row () {
            Object ();
            label = new Gtk.Label (null);
            check_button = new Gtk.CheckButton ();
            check_button.toggled.connect (() => {
                toggled (check_button.active);
            });

            append (check_button);
            append (label);
        }

        public void set_text (string text) {
            label.set_text (text);
        }

        public void set_state (ShoppingList.Item.State state) {
            if (state == ShoppingList.Item.State.UNCHECKED) {
                check_button.active = false;
            } else if (state == ShoppingList.Item.State.CHECKED) {
                check_button.active = true;
            }
        }

        public signal void toggled (bool active);
    }
}

