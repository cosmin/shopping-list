/* item.vala
 *
 * Copyright 2022 Cosmin Humeniuc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace ShoppingList {

    public class Item : GLib.Object {

        public enum State {
            BAD_STATE,
            UNCHECKED,
            CHECKED,
            AVAILABLE;

            private static State[] all () {
                return { BAD_STATE, UNCHECKED, CHECKED, AVAILABLE };
            }

            public static State value_of (int value) {
               foreach (var state in State.all ()) {
                   if (state == value) {
                       return state;
                   }
               }
               return BAD_STATE;
            }
        }

        public int64 id { get; construct; }
        public string name { get; set; }
        public State state { get; set; }

        public Item.with_id_and_name (int64 id, string name) {
            this.with_id_name_and_state (id, name, State.UNCHECKED);
        }

        public Item.with_id_name_and_state (int64 id, string name, State state) {
            Object (id: id);
            this.name = name;
            this.state = state;
        }
    }
}

