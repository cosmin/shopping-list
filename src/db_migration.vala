/* db_migration.vala
 *
 * Copyright 2022 Cosmin Humeniuc <cosmin@hume.ro>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace ShoppingList {

    class DbMigration : Object {

        private string path = Path.build_filename (Environment.get_user_data_dir (), "shopping-list", "shopping-list.db");

        public void run() {
            print ("Using database %s\n", path);
            DirUtils.create_with_parents (Path.get_dirname (path), 0775);

            Sqlite.Database db;
            int rc = Sqlite.OK;
            if ((rc = Sqlite.Database.open (path, out db)) != Sqlite.OK) {
                printerr ("Can't open database: %d, %s\n", rc, db.errmsg ());
                return;
            }

            int current_version = get_db_version (db);
            print ("Current database version is %d\n", current_version);

            try {
                var scripts_by_version = get_scripts_by_version ();
                foreach (var version_to_script in scripts_by_version) {
                    var version = version_to_script.key;
                    if (version > current_version) {
                        var script = version_to_script.value;
                        migrate (db, version, script);
                    }
                }
            } catch (Error e) {
                printerr ("Failed to load resource - %s\n", e.message);
            }

            print ("Migration complete\n");
        }

        private int get_db_version (Sqlite.Database db) {
            int rc = Sqlite.OK;
            Sqlite.Statement statement;
            if ((rc = db.prepare_v2 ("PRAGMA user_version", -1, out statement, null)) == 1) {
                printerr ("SQL error: %d, %s\n", rc, db.errmsg ());
                return -1;
            }

            if ((rc = statement.step()) != Sqlite.ROW) {
                printerr ("SQL error: %d, %s\n", rc, db.errmsg());
            }

            return statement.column_int (0);
        }

        private Gee.SortedMap<int, string> get_scripts_by_version () throws GLib.Error {
            var scripts_by_version = new Gee.TreeMap<int, string> ();
            string[] scripts = GLib.resources_enumerate_children ("/ro/hume/cosmin/ShoppingList/db/sql", ResourceLookupFlags.NONE);
            print ("Found %d migration scripts\n", scripts.length);
            foreach (var script in scripts) {
                var version = extract_version (script);
                scripts_by_version.@set (version, script);
            }
            return scripts_by_version;
        }

        private int extract_version (string name) {
            GLib.Regex pattern = /v([0-9]+)_.+/;
            GLib.MatchInfo match_info;
            if (pattern.match (name, 0, out match_info)) {
                var number = match_info.fetch (1);
                return int.parse (number);
            }

            return 0;
        }

        private void migrate (Sqlite.Database db, int version, string script) throws GLib.Error {
            print ("Applying migration script for version %d: %s\n", version, script);

            var bytes = GLib.resources_lookup_data("/ro/hume/cosmin/ShoppingList/db/sql/" + script, ResourceLookupFlags.NONE);
            uint8[] chars = bytes.get_data ();
            var query = (string) chars;
            print(">%s<\n", query);

            string error_message;

            int rc = db.exec ("BEGIN", null, out error_message);
	        if (rc != Sqlite.OK) {
		        printerr ("Error: %s\n", error_message);
		        return;
	        };

            rc = db.exec (query, null, out error_message);
	        if (rc != Sqlite.OK) {
		        printerr ("Error: %s\n", error_message);

                rc = db.exec ("ROLLBACK", null, out error_message);
	            if (rc != Sqlite.OK) {
		            printerr ("Error: %s\n", error_message);
	            };
		        return;
	        };

            rc = db.exec ("PRAGMA user_version = %d".printf (version), null, out error_message);
	        if (rc != Sqlite.OK) {
		        printerr ("Error: %s\n", error_message);

                rc = db.exec ("ROLLBACK", null, out error_message);
	            if (rc != Sqlite.OK) {
		            printerr ("Error: %s\n", error_message);
	            };
		        return;
	        };

            rc = db.exec ("COMMIT", null, out error_message);
	        if (rc != Sqlite.OK) {
		        printerr ("Error: %s\n", error_message);
		        return;
	        };
        }
    }
}
