/* db_accessor.vala
 *
 * Copyright 2022 Cosmin Humeniuc <cosmin@hume.ro>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace ShoppingList {

    class DbAccessor : Object {

        private string path = Path.build_filename (Environment.get_user_data_dir (), "shopping-list", "shopping-list.db");

        public int64 add_item (string name) {
            Sqlite.Database db;
            int rc = Sqlite.OK;
            if ((rc = Sqlite.Database.open (path, out db)) != Sqlite.OK) {
                printerr ("Can't open database: %d, %s\n", rc, db.errmsg ());
                return -1;
            }

            Sqlite.Statement statement;

            const string prepared_query_str = "INSERT INTO item (name, state) VALUES ($name, $state);";
            rc = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out statement);
            if (rc != Sqlite.OK) {
                stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
                return -1;
            }
            int name_param_position = statement.bind_parameter_index ("$name");
            assert (name_param_position > 0);

            int state_param_position = statement.bind_parameter_index ("$state");
            assert (state_param_position > 0);

            statement.bind_text (name_param_position, name);
            statement.bind_int (state_param_position, ShoppingList.Item.State.UNCHECKED);

            if ((rc = statement.step()) != Sqlite.DONE) {
                printerr ("Failed to insert item [%s]: %d, %s\n", name, rc, db.errmsg());
            }

            return db.last_insert_rowid ();
        }

        public List<Item> get_all_items () {
            Sqlite.Database db;
            int rc = Sqlite.OK;
            if ((rc = Sqlite.Database.open (path, out db)) != Sqlite.OK) {
                printerr ("Can't open database: %d, %s\n", rc, db.errmsg ());
                return new List<Item> ();
            }

            Sqlite.Statement statement;

            const string prepared_query_str = "SELECT id, name, state FROM item;";
            rc = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out statement);
            if (rc != Sqlite.OK) {
                stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
                return new List<Item> ();
            }

            List<Item> items = new List<Item> ();
            while (statement.step() == Sqlite.ROW) {
                int64 id = statement.column_int64 (0);
                string name = statement.column_text (1) ?? "<none>";
                int state_value = statement.column_int (2);
                var state = Item.State.value_of (state_value);
                var item = new Item.with_id_name_and_state (id, name, state);
                items.append (item);
            }
            print ("Read %u items\n", items.length ());
            return items;
        }

        public void update_item_state (int64 item_id, bool active) {
            Sqlite.Database db;
            int rc = Sqlite.OK;
            if ((rc = Sqlite.Database.open (path, out db)) != Sqlite.OK) {
                printerr ("Can't open database: %d, %s\n", rc, db.errmsg ());
                return;
            }

            Sqlite.Statement statement;

            const string prepared_query_str = "UPDATE item SET state = $state WHERE id = $id;";
            rc = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out statement);
            if (rc != Sqlite.OK) {
                stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
                return;
            }
            int state_param_position = statement.bind_parameter_index ("$state");
            assert (state_param_position > 0);

            int id_param_position = statement.bind_parameter_index ("$id");
            assert (id_param_position > 0);

            statement.bind_int64 (id_param_position, item_id);
            statement.bind_int (state_param_position, active ? ShoppingList.Item.State.CHECKED : ShoppingList.Item.State.UNCHECKED);

            if ((rc = statement.step()) != Sqlite.DONE) {
                printerr ("Failed to update item [%" + int64.FORMAT + "]: %d, %s\n", item_id, rc, db.errmsg());
            }
        }

        public void delete_item (int64 item_id) {
            Sqlite.Database db;
            int rc = Sqlite.OK;
            if ((rc = Sqlite.Database.open (path, out db)) != Sqlite.OK) {
                printerr ("Can't open database: %d, %s\n", rc, db.errmsg ());
                return;
            }

            Sqlite.Statement statement;

            const string prepared_query_str = "DELETE FROM item WHERE id = $id;";
            rc = db.prepare_v2 (prepared_query_str, prepared_query_str.length, out statement);
            if (rc != Sqlite.OK) {
                stderr.printf ("Error: %d: %s\n", db.errcode (), db.errmsg ());
                return;
            }
            int id_param_position = statement.bind_parameter_index ("$id");
            assert (id_param_position > 0);

            statement.bind_int64 (id_param_position, item_id);

            if ((rc = statement.step()) != Sqlite.DONE) {
                printerr ("Failed to delete item [%" + int64.FORMAT + "]: %d, %s\n", item_id, rc, db.errmsg());
            }
        }
    }
}
