/* application.vala
 *
 * Copyright 2022 Cosmin Humeniuc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace ShoppingList {

    public class Application : Adw.Application {

        public Application () {
            Object (application_id: "ro.hume.cosmin.ShoppingList", flags: ApplicationFlags.FLAGS_NONE);
        }

        construct {
            ActionEntry[] action_entries = {
                { "about", this.on_about_action },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", {"<primary>q"});
        }

        public override void activate () {
            base.activate ();
            new DbMigration().run();
            var win = this.active_window;
            if (win == null) {
                win = new ShoppingList.Window (this);
            }
            win.present ();
        }

        private void on_about_action () {
            string[] authors = { "Cosmin Humeniuc" };
            Gtk.show_about_dialog (this.active_window,
                                   "program-name", "Shopping List",
                                   "license-type", Gtk.License.GPL_3_0,
                                   "authors", authors,
                                   "version", "0.1.4.1",
                                   "logo-icon-name", "ro.hume.cosmin.ShoppingList",
                                   "website", "https://cosmin.hume.ro/project/shopping-list-gtk");
        }
    }
}
